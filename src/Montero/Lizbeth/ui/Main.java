package Montero.Lizbeth.ui;

import Montero.Lizbeth.tl.Controller;

import java.io.*;

public class Main {
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static Controller administrador = new Controller();

    public static void main(String[] args) throws IOException {
        mostrarMenu();
    }//FIN MAIN.

    public static void registrarEmpleado() throws IOException {
        out.println("Digite el nombre del empleado responsable");
        String nombre = in.readLine();
        out.println("Digite la cedula del empleado responsable");
        String cedula = in.readLine();
        if (administrador.buscarEmpleado(cedula) == -1) {
            administrador.registrarEmpleado(nombre, cedula);
            out.println("El empleado se ha resgistrado correctamente");
        } else {
            out.println("El empleado: " + nombre + " ya existe.");
        }
    }//FIN REGISTAR EMPLEADO

    public static void registrarCompu() throws IOException {
        out.println("Digite la serie de la computadora");
        String serie = in.readLine();
        out.println("Digite la marca de la computadora");
        String marca = in.readLine();
        out.println("Digite la cedula del empleado");
        String cedula = in.readLine();
        if (administrador.buscarComputadora(serie) == -1) {
            administrador.registrarComputadora(serie, marca);
            out.println("La computadora se ha resgistrado correctaente");
        } else {
            out.println("La computadora codigo: " + serie + " ya existe.");
        }
    }// FIN registarCompu

    public static void asociarEmpleadoAComputadora() throws IOException{
        out.print("Ingrese el número de serie de la computadora existente en el sistema: ");
        String serie = in.readLine();
        out.print("Digite el número de cedula del empleado registrado en el sistema : ");
        String cedula = in.readLine();

        if(administrador.asociarEmpleadoComputadora(serie,cedula)){
            out.println("La asociacion se a ejecutado exitosamente");
        } else {
            out.println("No se pudo asociar la computadora o empleado no existen en el sistema");
        }

    }
    public static void imprimirEmpleados() {
        String[] infoEmpleados = administrador.listarEmpleados();
        for (int i = 0; i < infoEmpleados.length; i++) {
            out.println(infoEmpleados[i]);
        }
    }
    public static void imprimirComputadoras() {
        String[] infoCompu = administrador.listarComputadoras();
        for (int i = 0; i < infoCompu.length; i++) {
            out.println(infoCompu[i]);
        }
    }
    public static void imprimirPc() {
        String[] infoPc = administrador.listarPc();
        for (int i = 0; i < infoPc.length; i++) {
            out.println(infoPc[i]);
        }
    }
    static void mostrarMenu() throws IOException {
        int opcion = -1;
        do {
            System.out.println("Menú");
            System.out.println("1. Registrar Empleado");
            System.out.println("2. Listar Empleados");
            System.out.println("3. Registrar Computadoras");
            System.out.println("4. Listar Computadoras");
            System.out.println("5. Asignar Computadora a Empleado Responsable");
            System.out.println("6. Listar Computadora Asignada a empleado");
            System.out.println("0. Salir");
            opcion = seleccionarOpcion();
            procesarOpcion(opcion);
        } while (opcion != 0);
    }

    /**
     * Rutina (función) que retorna el valor ingresado por el usuario.
     *
     * @return devuelve la opción seleccionada
     */
    static int seleccionarOpcion() throws IOException {
        System.out.println("Digite la opción");
        return Integer.parseInt(in.readLine());

    }
    static void procesarOpcion(int pOpcion) throws IOException {
        switch (pOpcion) {
            case 0:
                System.out.println("¡Aplicación cerrada exitosamente!.");
                break;
            case 1:
                registrarEmpleado();
                break;
            case 2:
                imprimirEmpleados();
                break;
            case 3:
                registrarCompu();
                break;
            case 4:
                imprimirPc();
                break;
            case 5:
                asociarEmpleadoAComputadora();
                break;
            case 6:
                imprimirComputadoras();
                break;
            default:
                System.out.println("Opción inválida");
                break;
        }
    }
}//FIN DE PROGRAMA.