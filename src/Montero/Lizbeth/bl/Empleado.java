package Montero.Lizbeth.bl;

/**
 * se inicializan los atributos
 */
public class Empleado {
    private String nombre, cedula;
    private Computadora compuAsigna;

    /**
     * contructor con los parametros propios de la clase
     * @param nombre tipo nombre, almacena el nombre del empleado.
     * @param cedula tipo string almacena la cedula del empleado
     */
    public Empleado(String nombre, String cedula) {
        this.nombre = nombre;
        this.cedula = cedula;
    }

    /**
     * Contructor con todos los parametros
     * @param nombre
     * @param cedula
     * @param compuAsigna
     */

    public Empleado(String nombre, String cedula, Computadora compuAsigna) {
        this.nombre = nombre;
        this.cedula = cedula;
        compuAsigna = compuAsigna;
    }

    /**
     * Contructor por defecto
     */
    public Empleado() {

    }
    /**
     * Metodo Gettter de la variable nombre es utilizado para obterner el atributo privado de la clase.
     *
     * @return devuelve  el nombre  del empleado ( lo vuleve publico) y de esta manera puede ser nombrado en otras calses.
     */
    public String getNombre() {
        return nombre;
    }
    /**
     * Metodo set es utilizado para asignar valores a los atributos privados de una clase.
     * @param nombre variable nombre del empleado ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    /**
     * Metodo Gettter de la variable cedula es utilizado para obterner el atributo privado de la clase.}
     *
     * @return devuelve  la cedula  del empleado ( lo vuleve publico) y de esta manera puede ser nombrado en otras calses.
     */
    public String getCedula() {
        return cedula;
    }
    /**
     * Metodo set es utilizado para asignar valores a los atributos privados de una clase.
     * @param cedula variable cedula del empleado ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }
    /**
     * Metodo Gettter de la variable computadora asignada es utilizado para obterner el atributo privado de la clase.}
     *
     * @return devuelve  la computadora asignada  al empleado ( lo vuleve publico) y de esta manera puede ser nombrado en otras calses.
     */
    public Computadora getCompuAsigna() {
        return compuAsigna;
    }
    /**
     * Metodo set es utilizado para asignar valores a los atributos privados de una clase.
     * @param compuAsigna variable computadora asignada al  empleado ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setCompuAsigna(Computadora compuAsigna) {
        this.compuAsigna = compuAsigna;
    }
    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     * @return retorna los valores respectivos del objeto, en este caso el respectivo valor de cada atributo de la clase Empleado en un solo String.
     */
    @Override
    public String toString() {
        return "Empleado{" +
                "nombre='" + nombre + '\'' +
                ", cedula='" + cedula + '\'' +
                '}';
    }}