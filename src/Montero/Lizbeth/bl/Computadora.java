package Montero.Lizbeth.bl;

import java.util.ArrayList;

public class Computadora {
    private String serie, marca;
    private Empleado empleadoResponsable;

    /**
     * Contructor por defecto
     */
    public Computadora() {
    }

    /**
     * Constructor con todos los parametros
     * @param serie guarda la serie de la compu, tipo string
     * @param marca guarda la marca de la comp, tipo String
     * @param empleadoResponsable guarda el empleado responsable, tipo Responsable.
     */
    public Computadora(String serie, String marca, Empleado empleadoResponsable) {
        this.serie = serie;
        this.marca = marca;
        this.empleadoResponsable = empleadoResponsable;
    }

    /**
     * Constructor que tiene todos los parametros propios de la clase
     * @param serie guarda la serie de la compu, tipo string
     * @param marca guarda la marca de la comp, tipo String
     */
    public Computadora(String serie, String marca) {
        this.serie = serie;
        this.marca = marca;
    }
    /**
     * Metodo set es utilizado para asignar valores a los atributos privados de una clase.
     * @param responsable variable que almacena el empleado responsable de la compu ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setResponsable(Empleado responsable) {
        empleadoResponsable = responsable;
    }
    /**
     * Metodo Gettter de la variable serie es utilizado para obterner el atributo privado de una clase.
     *
     * @return devuelve  la serie de la compu ( lo vuleve público) y de está manera puede ser nombrado en otras calses.
     */
    public String getSerie() {
        return serie;
    }
    /**
     * Metodo set es utilizado para asignar valores a los atributos privados de una clase.
     * @param serie variable que almacena la serie de la compu ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setSerie(String serie) {
        this.serie = serie;
    }
    /**
     * Metodo Gettter de la variable marca es utilizado para obterner el atributo privado de la clase.
     *
     * @return devuelve  la marca de la compu ( lo vuleve publico) y de esta manera puede ser nombrado en otras calses.
     */
    public String getMarca() {
        return marca;
    }
    /**
     * Metodo set es utilizado para asignar valores a los atributos privados de una clase.
     * @param marca variable que almacena la marca de la compu ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }
    /**
     * Metodo Gettter de la variable empleadoResponsable es utilizado para obterner el atributo privado de la clase.
     *
     * @return devuelve  el empleado responsable de la compu ( lo vuleve publico) y de esta manera puede ser nombrado en otras calses.
     */
    public Empleado getEmpleadoResponsable() {
        return empleadoResponsable;
    }
    /**
     * Metodo set es utilizado para asignar valores a los atributos privados de una clase.
     * @param empleadoResponsable variable el empleado responsable de la compu ingresado por el usuario en la capa ui y es registrado en la capa bl como privado en su calse respectiva.
     */
    public void setEmpleadoResponsable(Empleado empleadoResponsable) {
        this.empleadoResponsable = empleadoResponsable;
    }
    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     * @return retorna los valores respectivos del objeto, en este caso el respectivo valor de cada atributo de la clase Computadora en un solo String.
     */

    public String to() {
        return "Computadora{" +
                "serie='" + serie + '\'' +
                ", marca='" + marca + '\'' +
                '}';
    }


    /**
     * Metodo toString utilizado para imprimir la in información almacenada en un String asignado.
     * @return retorna los valores respectivos del objeto, en este caso el respectivo valor de cada atributo de la clase Computadora en un solo String.
     */
    @Override
    public String toString() {
        return "Computadora{" +
                "serie='" + serie + '\'' +
                ", marca='" + marca + '\'' +
                ", empleadoResponsable=" + empleadoResponsable +
                '}';
    }
}
