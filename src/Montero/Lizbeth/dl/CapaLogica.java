package Montero.Lizbeth.dl;

import Montero.Lizbeth.bl.Computadora;
import Montero.Lizbeth.bl.Empleado;

import java.util.ArrayList;

public class CapaLogica {

    private ArrayList<Computadora> computadoras;
    private ArrayList<Empleado> empleados;

    public CapaLogica() {
        computadoras = new ArrayList<>();
        empleados = new ArrayList<>();
    }

    public void registrarComputadora(Computadora computadora) {
        computadoras.add(computadora);
    }

    public int buscarComputadora(String numSerie) {
        int posicion = 0;
        for (Computadora compu : computadoras) {
            if (numSerie.equals(compu.getSerie())) {
                return posicion;
            }
            posicion++;
        }
        return -1;
    }

    public String getComputadora(int posicion) {
        return computadoras.get(posicion).toString();
    }

    public String getEmpleado(int posicion) {
        return empleados.get(posicion).toString();
    }

    public String[] listarComputadoras() {
        String[] datosComputadoras = new String[computadoras.size()];
        for (int x = 0; x < computadoras.size(); x++) {
            datosComputadoras[x] = computadoras.get(x).toString();
        }
        return datosComputadoras;
    }

    public String[] listarPc() {
        String[] datosPc = new String[computadoras.size()];
        for (int x = 0; x < computadoras.size(); x++) {
            datosPc[x] = computadoras.get(x).to();
        }
        return datosPc;
    }


    public void registrarEmpleado(Empleado empleado) {
        empleados.add(empleado);
    }

    public int buscarEmpleados(String cedula) {
        int posicion = 0;
        for (Empleado emplea : empleados) {
            if (cedula.equals(emplea.getCedula())) {
                return posicion;
            }
            posicion++;
        }
        return -1;
    }


    public String[] listarEmpleados() {
        String[] datosEmpleados = new String[empleados.size()];
        for (int x = 0; x < empleados.size(); x++) {
            datosEmpleados[x] = empleados.get(x).toString();
        }
        return datosEmpleados;
    }

    public void asociarEmpleadoComputadora(int posComputadora, int posEmpleado) {
        Empleado empleado = empleados.get(posEmpleado);
        computadoras.get(posComputadora).setResponsable(empleado);
    }

}